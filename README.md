## Description
Precor Connect product line service SDK for javascript.


## Features

##### List Product Lines
* [documentation](features/ListProductLines.feature)

## Setup

**install via jspm**  
```shell
jspm install product-line-service-sdk=bitbucket:precorconnect/product-line-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import ProductLineServiceSdk,{ProductLineServiceSdkConfig} from 'product-line-service-sdk'

const productLineServiceSdkConfig = 
    new ProductLineServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const productLineServiceSdk = 
    new ProductLineServiceSdk(
        productLineServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```